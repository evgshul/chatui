const chatApp = {
    data() {
        return {
            loginHeight:300,
            mDivTop:300,
            timer: 1000,
            isShow: true,
            isLogin: false,
            errorUsername:"",
            errorPass:"",
            messageText: null,
            messages: [],
            username:"",
            password:"",
            token:"",
            details:"",
            parameterViolations:"",
            path:"",
            message:""
        }
    },
    methods: {
       async login() {
            const {username, password} = this;
            if (this.errorPass !== "" || this.errorUsername !== "") {
                this.errorPass = "";
                this.errorUsername = "";
            }
            const res = await fetch(
               "https://chat-api.bug.guru/tokens", {
                   method: "POST",
                    headers:{
                        "Accept": "application/json",
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        username,
                        password
                    })
                }).then(r => {
                    if (r.ok) {
                        console.debug("login ok ");
                        this.isShow = false;
                        this.isLogin = true;
                        this.loginHeight = 0;
                        this.mDivTop = 1;
                        return r.json();
                    } else {
                        console.debug("wrong login");
                        return r.json();
                    }
            }).then(data => {
                console.debug("data receive ", data);
                const jsonParams = [];
                jsonParams.push(data);
                for (const item of jsonParams) {
                    if (item.token != null) {
                        console.debug("login success");
                        this.token = item.token;
                        this.refreshMessages();
                    } else {
                        if (item.parameterViolations != null) {
                            const errorPar = item.parameterViolations;
                            for (const i of errorPar) {
                                if (i.path.search("username") > 0) {
                                    console.error("wrong username");
                                    this.errorUsername = i.message;
                                    this.username = null;
                                }
                                if (i.path.search("password") > 0) {
                                    console.error("wrong password");
                                    this.errorPass = i.message;
                                    this.password = null;
                                }
                            }
                        } else {
                            console.error("incorect user", item.details)
                            this.details = item.details;
                            this.errorUsername = this.details;
                        }
                    }
                }
            })
        },
        sendMessage() {
            console.debug("Adding new Message", this.messageText);
            fetch("https://chat-api.bug.guru/messages", {
                method: "POST",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + this.token
                },
                body: JSON.stringify({
                    text: this.messageText
                })
            }).then(response => {
                if (response.ok) {
                    console.debug("OK");
                    return response.json();
                } else {
                    console.error("Sending message error")
                }
            }).then(data => {
                console.debug("adding message: ", data);
                this.refreshMessages();
                this.messages.push(data);
            });
            this.messageText = null;
            this.scroll();

        },
        refreshMessages() {
            fetch("https://chat-api.bug.guru/messages", {
                method: "GET",
                headers: {
                    "Accept" : "application/json"
                }
            }).then(response => {
                if (response.ok) {
                    console.debug("response is ok");
                    return response.json();
                } else {
                    console.debug("response isn't ok")
                }
            }).then(data => {
                console.debug("receive data" , data);
                this.messages = data;
                this.scroll();
            });
        }, scroll() {
            const md = this.$refs.messagesDiv;
            console.debug("scrolling", md);
            setTimeout(function (){
                md.scrollTop = md.scrollHeight;
            },500);
        }
    },
    mounted() {
        console.debug("App started..")
    }
}

Vue.createApp(chatApp).mount('#app')
